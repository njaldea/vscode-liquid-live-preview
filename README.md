# liquid-live-preview README

This plugin is created to help my wife learn HTML/CSS more while doing her work :)

## Features

- Context Menu in file explorer and editor for liquid files are added
- Once selected and vscode's state satisfies a certain criteria, a live preview for the liquid template is going to be spawned.
- Live preview can be accessed through popup modal.

## Requirements

- `.liquid` representing the template and `.json` representing the data with the same name. These are mandatory.
- no open editors. if there is an open editor, it should be the same file where the context menu came from.

## Extension Settings

## Known Issues

## Release Notes

### 0.0.1

Initial Release

### 0.0.2

Addition of context menu for editor
dependency fixes

### 0.0.3

dependency fixes
