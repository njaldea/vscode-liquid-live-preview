import * as vscode from 'vscode';
import { createServer, type Server } from 'http';
import { server as WebSocketServer } from 'websocket';
import { basename, dirname, join } from 'path';

import { Liquid } from 'liquidjs';
import { AddressInfo } from 'net';

let NJLA = vscode.window.createOutputChannel("NJLA");

const clientpage = (wsport: number) => 
`
<!DOCTYPE html>
<html lang="en">
<head/>
<body>
	<iframe id="iframe"></iframe>
	<style>
		html, body, #iframe {
			width: 100%;
			height: 100%;
		}
		*, *::before, *::after {
			box-sizing: border-box;
			padding: 0px;
			margin: 0px;
		}
		#iframe {
			overflow: hidden;
			display: block;
			border: none;
		}
	</style>
	<script defer>
		const iframe = document.getElementById("iframe");;
		const socket = new WebSocket("ws://0.0.0.0:${wsport}");
		socket.onmessage = function(e) {
			iframe.srcdoc = event.data;
			console.log(event);
		};
		// add reconnection
	</script>
</body>
</html>
`;

class Service {
	private template: vscode.TextEditor;
	private values: vscode.TextEditor;
	private engine: Liquid;
	private pageServer: Server;
	private pagePort: number;

	private wsServer: Server;
	private wsPort: number;
	private wss: WebSocketServer;

	private disposable: undefined | vscode.Disposable;

	constructor(template: vscode.TextEditor, values: vscode.TextEditor, type: string, title: string) {
		this.template = template;
		this.values = values;
		this.engine = new Liquid();

		this.pageServer = createServer();
		this.pageServer.on('request', (req, res) => {
			res.writeHead(200);
			res.end(clientpage(this.wsPort));
		});
		this.pageServer.listen(0);
		this.pagePort = (this.pageServer.address() as AddressInfo).port;

		this.wsServer = createServer();
		this.wsServer.listen(0);
		this.wsPort = (this.wsServer.address() as AddressInfo).port;

		this.wss = new WebSocketServer({
			httpServer: this.wsServer,
			autoAcceptConnections: true,
		});

		this.wss.on('connect', (connection) => {
			this.update(this.template.document.getText(), this.values.document.getText(), (content) => connection.send(content));
		});

		vscode.workspace.onDidChangeTextDocument(e => {
			if (this.template.document.fileName === e.document.fileName) {
				this.update(e.document.getText(), this.values.document.getText(), (content) => this.wss.broadcast(content));
			} else if (this.values.document.fileName === e.document.fileName) {
				this.update(this.template.document.getText(), e.document.getText(),  (content) => this.wss.broadcast(content));
			}
		});

		this.showMessage();
	}

	public async showMessage() {
		const result = await vscode.window.showInformationMessage(
			`live preview for ${this.template.document.fileName} is ready.`,
			"Open",
			"Ignore"
		);
		if (result === "Open") {
			vscode.env.openExternal(
				vscode.Uri.parse(`http://0.0.0.0:${this.pagePort}`)
			);
		}
	}

	public async update(template: string, values: string, send: (content: string) => void) {
		try {
			const v = JSON.parse(values);
			const tpl = this.engine.parse(template);
			send(await this.engine.render(tpl, v));
		} catch {
			// do nothing for now
		}
	}

	public dispose() {
		this.disposable?.dispose();
		this.disposable = undefined;
		this.wss.closeAllConnections();
		this.pageServer.close();
		this.wsServer.close();
	}

	public onDidDispose(cb: () => void) {
		this.disposable = vscode.workspace.onDidCloseTextDocument(
			e => {
				if (e.fileName === this.template.document.fileName || e.fileName === this.values.document.fileName) {
					this.dispose();
					cb();
				}
			}
		);
	}
}

async function openEditor(path: string) {
	try {
		const doc = await vscode.workspace.openTextDocument(path);
		return await vscode.window.showTextDocument(
			doc, { preview: false, viewColumn: vscode.ViewColumn.Beside }
		);
	} catch {} 
}

let service: Service | null = null;
export function activate(context: vscode.ExtensionContext) {
	context.subscriptions.push(
		vscode.commands.registerCommand('liquid-live-preview.preview', async (uri: vscode.Uri) => {

		if (service !== null) {
			service.showMessage();
			return;
		}

		let activeEditor = vscode.window.activeTextEditor ?? null;
		const tabGroups = vscode.window.tabGroups.all;
		if (activeEditor === null) {
			if (tabGroups.length > 0) {
				vscode.window.showErrorMessage("liquid-live-preview: close all editors");
				return;
			}
			activeEditor = await openEditor(uri.fsPath) as vscode.TextEditor;
		} else if (
			(activeEditor.document.fileName !== uri.fsPath) || // same file
			(tabGroups.length > 1 || tabGroups[0].tabs.length !== 1) // only 1 tab is open
		) {
			vscode.window.showErrorMessage("liquid-live-preview: close all editors");
			return;
		}

		const path = activeEditor.document.fileName;
		const valuesEditor = await openEditor(`${join(dirname(path), basename(path, '.liquid'))}.json`) ?? null;
		if (valuesEditor !== null) {
			service = new Service(activeEditor, valuesEditor, "liquid-live-preview", "liquid-live-preview-title");
			service.onDidDispose(() => service = null);
		} else {
			vscode.window.showErrorMessage("liquid-live-preview: No data found");
		}
	}));
}

// this method is called when your extension is deactivated
export function deactivate() {
	if (service) {
		service.dispose();
	}
}
